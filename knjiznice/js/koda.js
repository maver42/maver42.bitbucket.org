
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


var podatkiPac;

var bmiTab = [];
var bmiProc = [];

var bmiRazred = ["< 18.5 BMI (teža pod normalo)", "18.5 - 24.99 BMI (normalna teža)", "25 - 29.99 BMI (zvišana teža)", "30 - 34.99 BMI (debelost razred I)", 
"35 - 39.99 BMI (debelost razred II)", ">= 40 BMI (debelost razred III )"];

var bmiOsebka = [0, 0, 0, 0, 0, 0];


function generirajZacetne3UporabnikeOnLoad() {
  if(ehrIdTab.length != 0)
    return;
  var sporocilo = "";
  $("#prikazPodatkov").hide();
  ehrIdTemp = [];
    for(i=1; i <= 3; i++) {
        ehr = generirajPodatke(i);
        //console.log(ehr);
        ehrIdTemp.push(ehr);
        ehrIdTab.push(ehr);
    }
   
    sporocilo = "<span class='obvestilo label label-success fade-in'>Uspešno kreirani EHR: <br />";
    for(i=0; i < 3; i++) {
      sporocilo += ehrIdTemp[i].ehrIdPac + "<br /> ";
    }
    sporocilo +=  "</span>";
    $("#sporociloB").html(sporocilo);
    
    setTimeout(function() {
      $('#sporociloB').html('');
    }, 15000);
}

function generirajZacetne3Uporabnike() {
  var sporocilo = "";
  ehrIdTemp = [];
    for(i=1; i <= 3; i++) {
        ehr = generirajPodatke(i);
        console.log(ehr);
        ehrIdTemp.push(ehr);
        ehrIdTab.push(ehr);
    }
    console.log(ehrIdTab.length);
    
    sporocilo = "<span class='obvestilo label label-success fade-in'>Uspešno kreirani EHR: <br />";
    for(i=0; i < 3; i++) {
      sporocilo += ehrIdTemp[i].ehrIdPac + "<br /> ";
    }
    sporocilo +=  "</span>";
    $("#sporociloB").html(sporocilo);
    
    setTimeout(function() {
      $('#sporociloB').html('');
    }, 15000);
}

function generirajPodatke(stPacienta) {
    ehrId = "";
    podatkiPac = "";
    // TODO: Potrebno implementirati
    if(stPacienta === 1) {
        ehrId = zacetniEHRzaPacienta("Natsu", "Dragneel", "1995-01-02T13:23");
        $('#pacient').append('<a href="#" onclick="vnesiEHRID(\'' + ehrId + '\')">Natsu Dragneel</a>');
        podatkiPac = {ehrIdPac: ehrId, imePac: "Natsu", priimekPac: "Dragneel"};
        zacetneMeritve(ehrId, "2017-01-13T10:25", "180", "75");
        zacetneMeritve(ehrId, "2017-05-13T18:35", "181'", "76");
        zacetneMeritve(ehrId, "2017-09-13T14:17", "183", "77");
        zacetneMeritve(ehrId, "2017-12-13T07:17", "185", "79");
    } else if(stPacienta === 2) {
        ehrId = zacetniEHRzaPacienta("Erza", "Scarlet", "1988-05-07T08:35");
        $('#pacient').append('<a href="#" onclick="vnesiEHRID(\'' + ehrId + '\')">Erza Scasrlet</a>');
        podatkiPac = {ehrIdPac: ehrId, imePac: "Erza", priimekPac: "Scarlet"};
        //zacetniEHRzaPacienta(ehrId, "1975-06-13T01:25", "175", "60.00", "36.30", "120", "60", "98", "Micka" );
        zacetneMeritve(ehrId, "2017-01-13T10:25", "175", "50");
        zacetneMeritve(ehrId, "2017-05-13T18:35", "175'", "51");
        zacetneMeritve(ehrId, "2017-09-13T14:17", "175", "49");
        zacetneMeritve(ehrId, "2017-12-13T07:17", "175", "48");
    }else if(stPacienta === 3) {
        ehrId = zacetniEHRzaPacienta("Droy", "McGarden", "1968-12-25T22:20");
        $('#pacient').append('<a href="#" onclick="vnesiEHRID(\'' + ehrId + '\')">Droy McGarden</a>');
        podatkiPac = {ehrIdPac: ehrId, imePac: "Droy", priimekPac: "McGarden"};
        //zacetniEHRzaPacienta(ehrId, "1975-06-13T01:25", "175", "60.00", "36.30", "120", "60", "98", "Micka" );
        zacetneMeritve(ehrId, "2017-01-13T10:25", "182", "130");
        zacetneMeritve(ehrId, "2017-05-13T18:35", "182'", "133");
        zacetneMeritve(ehrId, "2017-09-13T14:17", "182", "140");
        zacetneMeritve(ehrId, "2017-12-13T07:17", "182", "145");
    }
    return podatkiPac;
}

function EHRzaPacienta() {
	sessionId = getSessionId();
	var ehrId = null;
	var ime = $("#vpisiIme").val();
	var priimek = $("#vpisiPriimek").val();
	var datumRojstva = $("#vpisiDatumR").val();
	podatkiPac = "";
	
	//console.log(ime + " " + priimek + " " + datumRojstva);

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#sporociloP").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
      
    	setTimeout(function() {
          $('#sporociloP').html('');
        }, 15000);
      
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    async: false,
		    success: function (data) {
		        ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            async: false,
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#sporociloP").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
                          
                          setTimeout(function() {
                            $('#sporociloP').html("");
                          }, 6000);
                          
		                    $("#ehrIdP").val(ehrId);
		                    podatkiPac = {ehrIdPac: ehrId, imePac: ime, priimekPac: priimek};
		                    ehrIdTab.push(podatkiPac);
		                    naloziPaciente();
		                }
		            },
		            error: function(err) {
		            	$("#sporociloP").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
                    
                    setTimeout(function() {
			          $('#sporociloP').html('');
			        }, 15000);
                    
		            }
		        });
		    }
		});
	}
}
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var sessionT = null;

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    if(sessionT != null)
      return sessionT;
    
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

var podatkiPac;

var ehrIdTemp = [];
var ehrIdTab = [];
var bmiTab = [];
var bmiProc = [];


var bmiRazred = ["< 18.5 BMI (teža pod normalo)", "18.5 - 24.99 BMI (normalna teža)", "25 - 29.99 BMI (zvišana teža)", "30 - 34.99 BMI (debelost razred I)", 
"35 - 39.99 BMI (debelost razred II)", ">= 40 BMI (debelost razred III )"];

var bmiOsebka = [0, 0, 0, 0, 0, 0];


function generirajZacetne3UporabnikeOnLoad() {
  if(ehrIdTab.length != 0)
    return;
  var sporocilo = "";
  $("#prikazPodatkov").hide();
  ehrIdTemp = [];
    for(i=1; i <= 3; i++) {
        ehr = generirajPodatke(i);
        //console.log(ehr);
        ehrIdTemp.push(ehr);
        ehrIdTab.push(ehr);
    }
    /*
    for(i=0; i <= 2; i++) {
        //ehr = generirajPodatke(i);
        console.log(ehrIdTab[i]);
        //ehrIdTab.push(ehr);
    }
    */
    sporocilo = "<span class='obvestilo label label-success fade-in'>Uspešno kreirani EHR: <br />";
    for(i=0; i < 3; i++) {
      sporocilo += ehrIdTemp[i].ehrIdPac + "<br /> ";
    }
    sporocilo +=  "</span>";
    $("#sporociloB").html(sporocilo);
    
    setTimeout(function() {
      $('#sporociloB').html('');
    }, 15000);
}

function generirajZacetne3Uporabnike() {
  var sporocilo = "";
  ehrIdTemp = [];
    for(i=1; i <= 3; i++) {
        ehr = generirajPodatke(i);
        console.log(ehr);
        ehrIdTemp.push(ehr);
        ehrIdTab.push(ehr);
    }
    console.log(ehrIdTab.length);
    /*
    for(i=0; i <= 2; i++) {
        //ehr = generirajPodatke(i);
        console.log(ehrIdTab[i]);
        //ehrIdTab.push(ehr);
    }
    */
    sporocilo = "<span class='obvestilo label label-success fade-in'>Uspešno kreirani EHR: <br />";
    for(i=0; i < 3; i++) {
      sporocilo += ehrIdTemp[i].ehrIdPac + "<br /> ";
    }
    sporocilo +=  "</span>";
    $("#sporociloB").html(sporocilo);
    
    setTimeout(function() {
      $('#sporociloB').html('');
    }, 15000);
}


function generirajPodatke(stPacienta) {
    ehrId = "";
    podatkiPac = "";
    // TODO: Potrebno implementirati
    if(stPacienta === 1) {
        ehrId = zacetniEHRzaPacienta("Natsu", "Dragneel", "1995-01-02T13:23");
        $('#pacient').append('<a href="#" onclick="vnesiEHRID(\'' + ehrId + '\')">Natsu Dragneel</a>');
        podatkiPac = {ehrIdPac: ehrId, imePac: "Natsu", priimekPac: "Dragneel"};
        zacetneMeritve(ehrId, "2017-01-13T10:25", "180", "75");
        zacetneMeritve(ehrId, "2017-05-13T18:35", "181'", "76");
        zacetneMeritve(ehrId, "2017-09-13T14:17", "183", "77");
        zacetneMeritve(ehrId, "2017-12-13T07:17", "185", "79");
    } else if(stPacienta === 2) {
        ehrId = zacetniEHRzaPacienta("Erza", "Scarlet", "1988-05-07T08:35");
        $('#pacient').append('<a href="#" onclick="vnesiEHRID(\'' + ehrId + '\')">Erza Scasrlet</a>');
        podatkiPac = {ehrIdPac: ehrId, imePac: "Erza", priimekPac: "Scarlet"};
        //zacetniEHRzaPacienta(ehrId, "1975-06-13T01:25", "175", "60.00", "36.30", "120", "60", "98", "Micka" );
        zacetneMeritve(ehrId, "2017-01-13T10:25", "175", "50");
        zacetneMeritve(ehrId, "2017-05-13T18:35", "175'", "51");
        zacetneMeritve(ehrId, "2017-09-13T14:17", "175", "49");
        zacetneMeritve(ehrId, "2017-12-13T07:17", "175", "48");
    }else if(stPacienta === 3) {
        ehrId = zacetniEHRzaPacienta("Droy", "McGarden", "1968-12-25T22:20");
        $('#pacient').append('<a href="#" onclick="vnesiEHRID(\'' + ehrId + '\')">Droy McGarden</a>');
        podatkiPac = {ehrIdPac: ehrId, imePac: "Droy", priimekPac: "McGarden"};
        //zacetniEHRzaPacienta(ehrId, "1975-06-13T01:25", "175", "60.00", "36.30", "120", "60", "98", "Micka" );
        zacetneMeritve(ehrId, "2017-01-13T10:25", "182", "130");
        zacetneMeritve(ehrId, "2017-05-13T18:35", "182'", "133");
        zacetneMeritve(ehrId, "2017-09-13T14:17", "182", "140");
        zacetneMeritve(ehrId, "2017-12-13T07:17", "182", "145");
    }
    return podatkiPac;
}

function EHRzaPacienta() {
	sessionId = getSessionId();
	var ehrId = null;
	var ime = $("#vpisiIme").val();
	var priimek = $("#vpisiPriimek").val();
	var datumRojstva = $("#vpisiDatumR").val();
	podatkiPac = "";
	
	//console.log(ime + " " + priimek + " " + datumRojstva);

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#sporociloP").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
      
    	setTimeout(function() {
          $('#sporociloP').html('');
        }, 15000);
      
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    async: false,
		    success: function (data) {
		        ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            async: false,
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#sporociloP").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
                          
                          setTimeout(function() {
                            $('#sporociloP').html("");
                          }, 6000);
                          
		                    $("#ehrIdP").val(ehrId);
		                    podatkiPac = {ehrIdPac: ehrId, imePac: ime, priimekPac: priimek};
		                    ehrIdTab.push(podatkiPac);
		                    naloziPaciente();
		                }
		            },
		            error: function(err) {
		            	$("#sporociloP").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
                    
                    setTimeout(function() {
			          $('#sporociloP').html('');
			        }, 15000);
                    
		            }
		        });
		    }
		});
	}
}


function vnesiEHRID(ehrId) {
  $("#ehrIdP").val(ehrId);
  $("#vpisiEhrId").val(ehrId);
}

function pridobiEhrPacienta() {
	sessionId = getSessionId();
	bmiTab = [];

	var ehrId = $("#ehrIdP").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#sporociloB").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
      
      setTimeout(function() {
          $('#sporociloB').html('');
        }, 15000);
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				
        setTimeout(function() {
          naloziPodatke();
        }, 200);
        
        $("#vrnisporociloP").html("<span class='obvestilo label " +
          "label-success fade-in'>Uspešno naloženi podatki bolnika '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
        
        $("#vrniIme").val(party.firstNames);
      	$("#vrniPriimek").val(party.lastNames);
      	$("#vrniDatumR").val(party.dateOfBirth);
      	$("#vrniEhrIdP").val(ehrId);
      	pridobiVisinoPacienta(ehrId);
          
        setTimeout(function() {
          $('#vrnisporociloP').html('');
        }, 15000);
			},
			error: function(err) {
				$("#vrnisporociloP").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
          
          setTimeout(function() {
          $('#vrnisporociloP').html('');
        }, 15000);
			}
		});
	}
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

/**
 *  Dropdown menu code 
 */
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */

function izbiraPacienta() {
    document.getElementById("pacient").classList.toggle("show");
}

function naloziPaciente() {
  if(ehrIdTab.length == 0)
    console.log("Ni pacientov");
  var k = 0;
  $('#pacient').html("");
  while(ehrIdTab.length > k) {
     $('#pacient').append('<a href="#" onclick="vnesiEHRID(\'' + ehrIdTab[k].ehrIdPac + '\')">' + ehrIdTab[k].imePac + ' ' + ehrIdTab[k].priimekPac + '</a>');
     k++;
  }
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
	if (!event.target.matches('.dropbtnP')) {

    var dropdowns = document.getElementsByClassName("dropdown-contentP");
    var i;
    //console.log(dropdowns);
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

/**
 * Funkciji za preklaplanje med prikazom in skrivanjem vnosnih polj in prikaza podatkov pacienta
 */
function naloziPodatke() {
  //console.log("nalagam podatke");
  $("#vnosPodatkov1").hide();
  
  $("#pacienti").hide();
  $("#prikazPodatkov1").show();
 
  $("#visinaP").hide();
  $("#telesnaTezaP").hide();
  
  $("#bmiP").hide();
  $("#bmiL").hide();
  $("#vrniGumb").show();
}

function nacin() {
  //console.log("vračam se na podatke");
  $("#visinaP").html('');
  $("#telesnaTezaP").html('');
 
  $("#bmiP").html('');
 
  $("#bmiL").html('');
  bmiTab = [];
  bmiProc = [];
 
  bmiOsebka = [0, 0, 0, 0, 0, 0];
  
  $("#vnosPodatkov1").show();
  
  $("#prikazPodatkov1").hide();
  
  $("#pacienti").show();
  $("#vrniGumb").hide();
}


function dodajMeritve() {
	//console.log("klic funkcije");
	sessionId = getSessionId();
	
	var ehrId = $("#vpisiEhrId").val();
	var datumInUra = $("#vpisiDatumMeritev").val();
	var telesnaVisina = $("#vpisiTelesnaVisna").val();
	var telesnaTeza = $("#vpisiTeza").val();


	if (!ehrId || ehrId.trim().length == 0) {
		$("#sporociloM").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
      
      setTimeout(function() {
          $('#sporociloM').html('');
        }, 15000);
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		    	//console.log("Uspešno dodani podatki");
		    	$("#vpisiEhrId").val('');
	        	$("#vpisiDatumMeritev").val('');
	        	$("#vpisiTelesnaVisna").val('');
	        	$("#vpisiTeza").val('');
		        		
		        $("#sporociloM").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno dodani podatki osebe z EHR '" +
                          ehrId + "'.</span>");
                          
                          setTimeout(function() {
                            $('#sporociloM').html("");
                          }, 6000);
		    },
		    error: function(err) {
		    	$("#sporociloM").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
            
	            setTimeout(function() {
		          $('#sporociloM').html('');
		        }, 15000);
		    }
		});
	}
}

function pridobiVisinoPacienta(ehrId) {
  sessionId = getSessionId();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#sporociloB").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
      
      setTimeout(function() {
          $('#sporociloB').html('');
        }, 15000);
        
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				
				//Telesna višina
				$.ajax({
			    url: baseUrl + "/view/" + ehrId + "/" + "height",
			    type: 'GET',
			    headers: {"Ehr-Session": sessionId},
			    success: function (res) {
			    	var visina = 0;
			    	if (res.length > 0) {
				    	var results = "<table class='table table-striped " +
                "table-hover'><tr><th>Datum in ura</th>" +
                "<th class='text-right'>Telesna višina</th></tr>";
				        for (var i in res) {
				            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].height +
                      " " + res[i].unit + "</td>";
				        }
				        results += "</table>";
				        $("#visinaP").append(results);
				        pridobiTezoPacienta(ehrId, res);
			    	} else {
			    		alert("Ni podatkov o teži pacienta!");
			    	}
			    },
			    error: function() {
			    	alert(JSON.parse(err.responseText).userMessage);
			    }
			   
			  });
			   prilepiBmi() ;
	    	},
			error: function(err) {
				$("#sporociloB").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
          
          setTimeout(function() {
	          $('#sporociloB').html('');
	        }, 15000);
        
			}
		});	
	}
}


function pridobiTezoPacienta(ehrId, resV) {
	if (!ehrId || ehrId.trim().length == 0) {
		$("#sporociloB").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
      
      setTimeout(function() {
          $('#sporociloB').html('');
        }, 15000);
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				
				//telesna teža
				$.ajax({
			    url: baseUrl + "/view/" + ehrId + "/" + "weight",
			    type: 'GET',
			    headers: {"Ehr-Session": sessionId},
			    success: function (res) {
			    	var bmi = 0;
			    	if (res.length > 0) {
				    	var results = "<table class='table table-striped " +
                "table-hover'><tr><th>Datum in ura</th>" +
                "<th class='text-right'>Telesna teža</th></tr>";
				        for (var i in res) {
				            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].weight +
                      " " + res[i].unit + "</td>";
                      	bmi = vrniBmi(resV[i].height, res[i].weight);
                      	bmiTab.push(bmi);
				        }
				        results += "</table>";
				        $("#telesnaTezaP").append(results);
			    	} else {
			    		alert("Ni podatkov o višini pacienta!");
			    	}
			    },
			    error: function() {
			    	alert(JSON.parse(err.responseText).userMessage);
			    }
			  });
					
				//klic funkcije za pridobitev tlaka
				prilepiBmi() ;
			},
			error: function(err) {
				$("#sporociloB").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
          
          setTimeout(function() {
	          $('#sporociloB').html('');
	        }, 15000);
	        
			}
		});
		prilepiBmi() ;
	}
}

function vrniBmi(visina, teza) {
	var bmiT = 0;
	bmiT = teza*10000/(visina*visina);
	return bmiT.toFixed(2);
}


function pridobiGraf(ehrId) {
  sessionId = getSessionId();
	$.ajax({
		url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
		type: 'GET',
		headers: {"Ehr-Session": sessionId},
	   	success: function (data) {
			var party = data.party;
			prilepiBmi() ;
					
			}
		
		});
	
}




function prilepiBmi() {
    for (var i in bmiTab) {
    	if(bmiTab[i] < 18.5)
    		bmiOsebka[0]++;
    	else if(bmiTab[i] >= 18.5 && bmiTab[i] < 24.99)
    		bmiOsebka[1]++;
        else if(bmiTab[i] > 25 && bmiTab[i] <= 29.99)
    		bmiOsebka[2]++;
    	else if(bmiTab[i] >= 30 && bmiTab[i] <= 34.99)
    		bmiOsebka[3]++;
    	else if(bmiTab[i] >= 35 && bmiTab[i] <= 39.99)
    		bmiOsebka[4]++;	
    	else if(bmiTab[i] >= 40)
    		bmiOsebka[5]++;
    }
    
    for(i = 0; i < 6; i++) {
    	povpBmi = (bmiOsebka[i]/bmiTab.length)*100;
    	bmiProc.push(povpBmi.toFixed(2));
    }
    showGraf("bmi", bmiProc, bmiRazred);
}

function showGraf(imeApp, tabProc, tabRazred) {
	var salesData =[];
	var podatki=[];
	
	for(i in tabProc) {
		if(tabProc[i] > 0) {
			if(tabProc[i] == 100.00)
				tabProc[i] = 99;
			var color = getRandomColor();
			var data1 = {color: color, value: tabProc[i]};
			salesData.push(data1);
			var podatki1 = {color: color, value: i};
			podatki.push(podatki1);
		}
	}
	
	var svg = d3.select("#" + imeApp + "P").append("svg").attr("width",700).attr("height",300);
	svg.append("g").attr("id", imeApp+"Donut");
	Donut3D.draw(imeApp+"Donut", randomData(), 150, 150, 130, 100, 30, 0.4);
	//console.log(salesData);
	//console.log(podatki);
	
	
	var results = "<table class='table table-striped " +
                "table-hover'><tr><th>Legenda: </th>";
	for(i in podatki) {
		results += "<tr><td style='color:white; background-color:"+ podatki[i].color 
		+ "'>" + tabRazred[podatki[i].value] +
                      "</td>";
	}
	results += "</table>";
	$("#" + imeApp + "L").append(results);
	
	
	function randomData(){
		return salesData.map(function(d){ 
			return {value:d.value, color:d.color};});
	}

}


function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function spremeniTekst(idButton) {
	if($("#"+idButton).val() == 'Prikaži')
		$("#"+idButton).val('Skrij');
	else if($("#"+idButton).val() == 'Skrij')
		$("#"+idButton).val('Prikaži');
}



function vnesiEHRID(ehrId) {
  $("#ehrIdP").val(ehrId);
  $("#vpisiEhrId").val(ehrId);
}

function pridobiEhrPacienta() {
	sessionId = getSessionId();
	bmiTab = [];

	var ehrId = $("#ehrIdP").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#sporociloB").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
      
      setTimeout(function() {
          $('#sporociloB').html('');
        }, 15000);
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				
        setTimeout(function() {
          naloziPodatke();
        }, 200);
        
        $("#vrnisporociloP").html("<span class='obvestilo label " +
          "label-success fade-in'>Uspešno naloženi podatki bolnika '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
        
        $("#vrniIme").val(party.firstNames);
      	$("#vrniPriimek").val(party.lastNames);
      	$("#vrniDatumR").val(party.dateOfBirth);
      	$("#vrniEhrIdP").val(ehrId);
      	pridobiVisinoPacienta(ehrId);
          
        setTimeout(function() {
          $('#vrnisporociloP').html('');
        }, 15000);
			},
			error: function(err) {
				$("#vrnisporociloP").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
          
          setTimeout(function() {
          $('#vrnisporociloP').html('');
        }, 15000);
			}
		});
	}
}



// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

/**
 *  Dropdown menu code 
 */
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */

function izbiraPacienta() {
    document.getElementById("pacient").classList.toggle("show");
}

function naloziPaciente() {
  if(ehrIdTab.length == 0)
    console.log("Ni pacientov");
  var k = 0;
  $('#pacient').html("");
  while(ehrIdTab.length > k) {
     $('#pacient').append('<a href="#" onclick="vnesiEHRID(\'' + ehrIdTab[k].ehrIdPac + '\')">' + ehrIdTab[k].imePac + ' ' + ehrIdTab[k].priimekPac + '</a>');
     k++;
  }
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
	if (!event.target.matches('.dropbtnP')) {

    var dropdowns = document.getElementsByClassName("dropdown-contentP");
    var i;
    //console.log(dropdowns);
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

/**
 * Funkciji za preklaplanje med prikazom in skrivanjem vnosnih polj in prikaza podatkov pacienta
 */
function naloziPodatke() {
  //console.log("nalagam podatke");
  $("#vnosPodatkov1").hide();
  $("#vnosPodatkov2").hide();
  $("#pacienti").hide();
  $("#prikazPodatkov1").show();
  $("#prikazPodatkov2").show();
  $("#prikazPodatkov3").show();
  $("#visinaP").hide();
  $("#telesnaTezaP").hide();

  $("#bmiP").hide();
  $("#bmiL").hide();
  $("#vrniGumb").show();
}

function nacin() {
  //console.log("vračam se na podatke");
  $("#visinaP").html('');
  $("#telesnaTezaP").html('');
 
  $("#bmiP").html('');

  $("#bmiL").html('');
  bmiTab = [];
  bmiProc = [];

  bmiOsebka = [0, 0, 0, 0, 0, 0];

 
  $("#vnosPodatkov1").show();
  $("#vnosPodatkov2").show();
  $("#prikazPodatkov1").hide();
  $("#prikazPodatkov2").hide();
  $("#prikazPodatkov3").hide();
  $("#pacienti").show();
  $("#vrniGumb").hide();
}


function dodajMeritve() {
	//console.log("klic funkcije");
	sessionId = getSessionId();
	
	var ehrId = $("#vpisiEhrId").val();
	var datumInUra = $("#vpisiDatumMeritev").val();
	var telesnaVisina = $("#vpisiTelesnaVisna").val();
	var telesnaTeza = $("#vpisiTeza").val();
	

	if (!ehrId || ehrId.trim().length == 0) {
		$("#sporociloM").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
      
      setTimeout(function() {
          $('#sporociloM').html('');
        }, 15000);
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		 
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		    	//console.log("Uspešno dodani podatki");
		    	$("#vpisiEhrId").val('');
	        	$("#vpisiDatumMeritev").val('');
	        	$("#vpisiTelesnaVisna").val('');
	        	$("#vpisiTeza").val('');
	        
	        	preveri('1');
		        		
		        $("#sporociloM").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno dodani podatki osebe z EHR '" +
                          ehrId + "'.</span>");
                          
                          setTimeout(function() {
                            $('#sporociloM').html("");
                          }, 6000);
		    },
		    error: function(err) {
		    	$("#sporociloM").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
            
	            setTimeout(function() {
		          $('#sporociloM').html('');
		        }, 15000);
		    }
		});
	}
}

function pridobiVisinoPacienta(ehrId) {
  sessionId = getSessionId();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#sporociloB").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
      
      setTimeout(function() {
          $('#sporociloB').html('');
        }, 15000);
        
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				
				//Telesna višina
				$.ajax({
			    url: baseUrl + "/view/" + ehrId + "/" + "height",
			    type: 'GET',
			    headers: {"Ehr-Session": sessionId},
			    success: function (res) {
			    	var visina = 0;
			    	if (res.length > 0) {
				    	var results = "<table class='table table-striped " +
                "table-hover'><tr><th>Datum in ura</th>" +
                "<th class='text-right'>Telesna višina</th></tr>";
				        for (var i in res) {
				            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].height +
                      " " + res[i].unit + "</td>";
				        }
				        results += "</table>";
				        $("#visinaP").append(results);
				        pridobiTezoPacienta(ehrId, res);
			    	} else {
			    		alert("Ni podatkov o teži pacienta!");
			    	}
			    },
			    error: function() {
			    	alert(JSON.parse(err.responseText).userMessage);
			    }
			  });
	    	},
			error: function(err) {
				$("#sporociloB").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
          
          setTimeout(function() {
	          $('#sporociloB').html('');
	        }, 15000);
        
			}
		});	
	}
}


function pridobiTezoPacienta(ehrId, resV) {
	if (!ehrId || ehrId.trim().length == 0) {
		$("#sporociloB").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
      
      setTimeout(function() {
          $('#sporociloB').html('');
        }, 15000);
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				
				//telesna teža
				$.ajax({
			    url: baseUrl + "/view/" + ehrId + "/" + "weight",
			    type: 'GET',
			    headers: {"Ehr-Session": sessionId},
			    success: function (res) {
			    	var bmi = 0;
			    	if (res.length > 0) {
				    	var results = "<table class='table table-striped " +
                "table-hover'><tr><th>Datum in ura</th>" +
                "<th class='text-right'>Telesna teža</th></tr>";
				        for (var i in res) {
				            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].weight +
                      " " + res[i].unit + "</td>";
                      	bmi = vrniBmi(resV[i].height, res[i].weight);
                      	bmiTab.push(bmi);
				        }
				        results += "</table>";
				        $("#telesnaTezaP").append(results);
			    	} else {
			    		alert("Ni podatkov o višini pacienta!");
			    	}
			    },
			    error: function() {
			    	alert(JSON.parse(err.responseText).userMessage);
			    }
			  });
					
				//klic funkcije za pridobitev tlaka
				pridobiGraf(ehrId);
			},
			error: function(err) {
				$("#sporociloB").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
          
          setTimeout(function() {
	          $('#sporociloB').html('');
	        }, 15000);
	        
			}
		});
	}
}

function vrniBmi(visina, teza) {
	var bmiT = 0;
	bmiT = teza*10000/(visina*visina);
	return bmiT.toFixed(2);
}



function prilepiBmi() {
    for (var i in bmiTab) {
    	if(bmiTab[i] < 18.5)
    		bmiOsebka[0]++;
    	else if(bmiTab[i] >= 18.5 && bmiTab[i] < 24.99)
    		bmiOsebka[1]++;
        else if(bmiTab[i] > 25 && bmiTab[i] <= 29.99)
    		bmiOsebka[2]++;
    	else if(bmiTab[i] >= 30 && bmiTab[i] <= 34.99)
    		bmiOsebka[3]++;
    	else if(bmiTab[i] >= 35 && bmiTab[i] <= 39.99)
    		bmiOsebka[4]++;	
    	else if(bmiTab[i] >= 40)
    		bmiOsebka[5]++;
    }
    
    for(i = 0; i < 6; i++) {
    	povpBmi = (bmiOsebka[i]/bmiTab.length)*100;
    	bmiProc.push(povpBmi.toFixed(2));
    }
    showGraf("bmi", bmiProc, bmiRazred);
}


function showGraf(imeApp, tabProc, tabRazred) {
	var salesData =[];
	var podatki=[];
	
	for(i in tabProc) {
		if(tabProc[i] > 0) {
			if(tabProc[i] == 100.00)
				tabProc[i] = 99;
			var color = getRandomColor();
			var data1 = {color: color, value: tabProc[i]};
			salesData.push(data1);
			var podatki1 = {color: color, value: i};
			podatki.push(podatki1);
		}
	}
	
	var svg = d3.select("#" + imeApp + "P").append("svg").attr("width",700).attr("height",300);
	svg.append("g").attr("id", imeApp+"Donut");
	Donut3D.draw(imeApp+"Donut", randomData(), 150, 150, 130, 100, 30, 0.4);
	//console.log(salesData);
	//console.log(podatki);
	
	
	var results = "<table class='table table-striped " +
                "table-hover'><tr><th>Legenda: </th>";
	for(i in podatki) {
		results += "<tr><td style='color:white; background-color:"+ podatki[i].color 
		+ "'>" + tabRazred[podatki[i].value] +
                      "</td>";
	}
	results += "</table>";
	$("#" + imeApp + "L").append(results);
	
	
	function randomData(){
		return salesData.map(function(d){ 
			return {value:d.value, color:d.color};});
	}

}


function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function spremeniTekst(idButton) {
	if($("#"+idButton).val() == 'Prikaži')
		$("#"+idButton).val('Skrij');
	else if($("#"+idButton).val() == 'Skrij')
		$("#"+idButton).val('Prikaži');
}